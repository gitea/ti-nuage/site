.ND 06/10/2022
.TL
Compte-rendu de l'Assemblée Générale
.br
du jeudi 6 octobre 2022 à Vieux-Marché
.AU "David, Mathias"
.MT 4
Les membres de l'association
.B "Ti Nuage"
ont été convoqués par courrier électronique le 12 septembre 2022 pour une Assemblée Générale ce jeudi 6 octobre, à 20 heures à la Hutte, 24 place Anjela Duval à Vieux-Marché.
L'objet de cette réunion est de discuter du bilan de l'année écoulée et de dresser les objectifs pour la suivante. Cette réunion voit aussi l'arrivée d'un nouveau membre : Gilles.
.P
Le Président de séance est Nourdine, le secrétaire ; Mathias.
.hl
.PRESENCE "Présent(e)s" "7 membres physiques (David, Gilles, Julie, Mathias, Nourdine, René, Vincent L.), 1 membre moral : Liratouva (représenté par Julie)"
.PRESENCE "Excusé(e)s" "Emmanuelle, Franck, Gérard, Mélina"
.PRESENCE "Absent(e)s" "Amicale laïque de Bégard, La Convergence des Loutres, Tous en scène, Antoine, Thomas, Grégoire, Sara, Jaques, Annia, Cécile, Tristan, Ninon, Luc, Michael, Jean-Luc, Vincent T."
.HU "Modification des statuts"
Selon le souhait du Président de permettre à tous de pouvoir participer à la vie de l'association sans restriction, il est tout d'abord proposé d'amender l'article 8 des statuts afin de supprimer certaines limites quant au vote des membres.
Précisément, la proposition consiste à supprimer, au sous-titre
.I "Électeurs" ,
la phrase : «
.I "Seuls les membres adhérents depuis plus de 6 mois à l’association sont autorisés à voter" ».
.P
La proposition, soumise au vote, est approuvée par quatre voix.
Trois membres s'abstiennent, n'ayant pas d'avis arrêté sur la question et faisant remarquer avec justesse qu'il faudra à l'avenir remplacer cette mention par une mesure permettant de protéger l'association de toute prise de pouvoir intempestive.
.HU "Bilan moral"
L'association, déclarée à la sous-préfecture de Lannion le 18 octobre 2021, a presque un an d'existence.
Nourdine informe l'Assemblée que
.B "Ti Nuage"
compte désormais 22 adhérents physiques et 4 adhérents moraux (chacun de ces derniers ayant 2 comptes en moyenne), soit un total de 30 comptes.
David précise que la taille de l'association croît lentement et régulièrement, avec environ deux nouveaux adhérents chaque mois, ce qui est tout à fait soutenable pour un futur proche prévisible au regard de la situation technique actuelle.
.P
Durant l'annéee écoulée, l'association a participé aux rencontres publiques suivantes :
.BL
.LI
.I "Libre en fête"
\*F
.FS
.URL "https://libre-en-fete.net"
.FE
le 20 mars à Vieux Marché, pour laquelle quelque membres ont assuré une permanence durant l'après-midi ;
.LI
une rencontre à l'initiative du FabLab de Lannion le 26 mars pour
.I "Libre en fête"
encore, afin de discuter des services numériques libres et présenter
.B "Ti Nuage"
;
.LI
le forum des associations à Plouaret le 3 septembre 2022.
.LE
.P
En début d'année, elle a été présente dans les média suivant :
.BL
.LI
le Télégramme du 3 février 2022 ;
.LI
dans une pastille sur Océane FM le 2 mars 2022 ;
.LI
le Trégor le 17 mars à l'occasion de
.I "Libre en fête" .
.LE
.P
La première moitié de l'année a été largement consacrée à la stabilisation de l'offre de services, à la mise en place du site, à la documentation des procédures pour les administrateurs.
Deux grands dossiers ont ensuite occupé l'essentiel du temps du bureau.
.P
Le premier est notre candidature aux CHATONS\*F
.FS
.URL "https://www.chatons.org"
.FE
qui est en passe d'être revue.
Nous aurons en novembre une période pour prendre en considération les commentaires faits sur notre candidature, puis les votes auront lieu en décembre.
.P
Le second dossier est la réponse à l'Appel à projets
.I "Construire le numérique dans les Côtes d'Armor"
\*F
.FS
.URL "https://cotesdarmor.fr/numerique"
.FE
du département.
Le budget est voté ce mois d'octobre et les résultats seront annoncés le 25 novembre, durant une cérémonie à laquelle nous sommes invités.
.P
À l'occasion du dossier pour le département, il a été nécessaire de réaliser une courte vidéo pour présenter notre projet.
La vidéo, réalisée par Vincent, s'avère être un remarquable document pour communiquer au sujet de l'association.
.B "Ti Nuage"
a donc adhéré à
.B "TeDomum"
\*F
.FS
.URL "https://www.tedomum.net"
.FE
qui propose une instance
.I "Peer Tube"
afin de publier cette vidéo, et potentiellement d'autres à l'avenir.
\R'Hu +1'
.HU "Perspectives"
Nourdine soumet l'idée d'ouvrir des comptes de courrier électroniques (courrier et agenda pour un stockage de l'ordre de 100Mo) à certains non adhérents et Mathias expose un cas pratique.
L'idée est accueillie favorablement.
.P
Les efforts immédiats visent à améliorer la vitrine de l'association et sa documentation pour satisfaire au mieux la charte des CHATONS et répondre à toute remarque de leur part.
.P
Si le déploiement de la fibre se poursuit comme attendu, les prochains investissements de l'association devraient avoir lieu en début d'année prochaine pour commencer à installer du matériel à la Hutte.
.P
Nous espérons pour cela beaucoup du dossier remis au département.
Cependant, le dossier ne couvre pas tout le financement, et il convient de toutes manières de chercher encore des subventions, car si le budget est équilibré pour la maintenance des services l'association ne dispose pas de moyens forts d'investissement.
L'organisme public le plus concerné par nos activités et le plus à même de nous répondre rapidement est Lannion Trégor-Communauté.
Il convient de réunir les possibilités de contact de la part de Nourdine, de Julie ou de Vincent T. et de les rencontrer dans les mois qui viennent.
.P
.B "Ti Nuage"
est invitée à une manifestation publique à Plougrescant le 4 mars 2023 :
.I "Faites du numérique" .
Cet événement doit être organisé par une personne qui comme nous a répondu à l'appel du département.
Sa tenue dépendra donc du résultat de l'attribution des budgets.
.P
Nourdine fait part de son souhait de se rendre au prochain Camp CHATONS durant l'été 2023, et peut-être de prélever un budget pour cela sur le compte de l'association.
Ces camps sont l'occasion de discuter durant un week-end de technique, de communication, d'éthique, de legislation, de sensibilisation au libre, etc.
L'Association est favorable à l'idée.
.HU "Vote"
Le bilan moral est approuvé à l'unanimité.
\R'Hu -1'
.HU "Rapport financier"
Les finances de l'association sont saines, l'année se termine par un (petit) bénéfice d'environ 550€.
Il convient de noter que ce
.I "bénéfice"
n'est pas dû à un trop gros montant des cotisations, mais devrait s'atténuer avec la montée en charge des services, toutes choses égales par ailleurs.
.TS
allbox centre tab(|);
lw(8cm)nene.
Adhésions|27|968,00
Assurance|1|87,53
Dépenses diverses|3|59,01
Dons|2|105,00
Frais bancaires|13|109,60
Frais postaux|1|13,92
Hébergeur|11|251,27
.TE
\R'Hu +1'
.HU "Vote"
Le rapport financier est approuvé à l'unanimité.
.P
Il est également voté de reconduire le montant actuel des cotisations.
\R'Hu -1'
.HU "Axes de réflexion"
L'assemblée soulève la question de la pérennité de
.B "Ti Nuage"
dans le confort de tous, en particulier de son Président sur lequel repose l'essentiel des tâches.
Celui-ci explique que l'association a besoin d'un administrateur supplémentaire pour les opérations de maintenance ordinaire, en particulier la surveillance de l'infrastructure de courriel qui nécessite une certaine réactivité.
.P
Si aucun nouvel adhérent n'est prêt à s'investir, les possibilités entrevues sont l'ouverture d'un poste d'administrateur système à temps partiel ou la facturation de services supplémentaires.
Ces services pourraient rémunérer les administrateurs ou être réalisés par des auto-entrepreneurs locaux, qui participeraient alors à la maintenance des services.
Toutes ces idées ont déjà fait l'objet de discussions, et il est d'avis qu'elles nécessitent d'être encore mûries.
.P
Gilles propose de réfléchir à l'idée de postes détachés d'entreprise : un type de poste non rémunéré par l'association bénéficiaire et pouvant apporter un temps de travail assez important.
La proposition mérite d'être davantage débattue sur le principe (Sommes-nous prêt à risquer de promouvoir du
.I "social-washing"
?
Comment doit être encadré le partenariat ?), et sur les implications en terme de travail pour les membres ; constitution du dossier de candidature, définition de la mission du salarié, suivi des activités, etc.
.P
Gilles enfin nous invite à contacter l'association
.B "Code d'Armor"
\*F
.FS
.URL "https://codedarmor.fr"
.FE
qui, au delà de sa vitrine de
.I "Google Developers Group" ,
constitue un réseau local de personnes propres à nous aider peut-être en terme de visibilité, d'organisation ou de matériel.
.HU "Renouvellement du Bureau"
L'Assemblée vote à l'unanimité la reconduction de Nourdine en tant que Président.
David, unique candidat au poste de secrétaire est élu également à l'unanimité.
Mathias, unique candidat au poste de trésorier est enfin aussi élu à l'unanimité.
Gilles et Vincent L. font part de leur intérêt à intégrer le Bureau qui se compose donc maintenant comme suit :
.BL
.LI
Président : Nourdine Gernelle
.LI
Secrétaire : David Soulayrol
.LI
Trésorier : Mathias Mantello
.LI
Membres ordinaires : Gilles Falcon, Vincent Lhoutellier
.LE
.SP 4
.P
L'Assemblée est levée à 22h30 par épuisement de l'ordre du jour.
