.ND 22/05/2022
.TL
Compte-rendu de Réunion
.br
Mardi 17 mai 2022 à Vieux-Marché
.AU "David, Mathias et Nourdine"
.AST "Résumé"
.AS
Outre le petit noyau de personnes habituel, nous avons eu le plaisir de faire connaissance avec Ninon, adhérente récente, et de discuter de ses envies et de ses connaissances au sujet de
.I Wordpress
et des différentes solutions pour réaliser un site marchand.
.P
Les administrateurs continuent le travail pour proposer
.B "Ti Nuage"
lors de la prochaine portée des CHATONS\*F
.FS
Le Collectif des Hébergeurs Alternatifs, Transparents, Neutres et Solidaires\ :
.URL https://www.chatons.org
.FE
et s'interrogent sur l'évolution des quotas, et la modération des services publics et anonymes.
Il est aussi question d'essayer d'autre outils de messagerie instantanée que Mattermost.
.P
Nous essayons de mettre en place un rendez-vous récurrent avec les adhérents.
La prochaine réunion aura lieu entre le mardi 28 juin et le vendredi 1er juillet et sera annoncée ici.
Les adhérents sont invités sur la liste de diffusion interne à choisir la date la plus appropriée et à soumettre leurs questions.
.AE
.MT 4
.hl
.PRESENT "David, Mathias, Ninon, Nourdine, Vincent L."
.HU "Quotas pour les comptes de groupe"
L'association
.B Liratouva
possède 4 comptes et rencontre fréquemment des problèmes pour l'hébergement des courriels.
Nourdine propose de faire évoluer les quotas pour les associations de manière à privilégier l'espace dédié au partage et à la communication :
.BL
.LI
le quota pour les courriels s'accroît de 500Mo à 3 Go ;
.LI
l'espace Nextcloud personnel est réduit de 3Go à 100Mo ;
.LI
le répertoire partagé entre les différents comptes passe de 5Go à 10Go.
.LE
.P
L'
.B "Amicale Laïque de Bégard"
(ALB) est actuellement la seule autre association hébergée.
Comme elle ne possède qu'un seul compte, il est décidé pour aller dans le même sens en portant le quota de son espace NextCloud de 3Go à 5Go.
.P
À propos de quotas, il est rappelé que la politique pour l'hébergement Web est de ne pas imposer de quotas tant qu'il n'y a pas d'abus.
.HU "Bulletin d'adhésion papier"
Sur le modèle de celui de l'association
.B Liratouva ,
Nourdine propose un nouveau document pour les inscriptions.
Le document est approuvé à une correction près.
Il est stocké à l'adresse
.URL "https://cloud.ti-nuage.fr/s/Ro3iprNBakNF4cW" .
\&.
.HU "CHATONS"
Le travail pour être candidat à la prochaine portée avance.
Les questions suivantes sont levées par Nourdine et David.
.P
.BL
.LI
Faut-il déménager physiquement les serveurs au préalable\ ?
Avoir une ou des machines sur Plouaret (parce que le VDSL est disponible sur cette commune) ou ailleurs\ ?
Il est décidé que ce point n'est pas bloquant pour la candidature.
.LI
Si on garde l'hébergement chez OVH, comment se passer de
.I MailJet
qui est un élément bloquant pour adhérer à la charte des CHATONS\ ?
L'utilisation d'un VPN pour le moment (
.I FAIMaison ,
.I FDN ,
ou
.URL https://grifon.fr
) est envisagée et doit être testée.
.LI
Doit-on conserver, dans la charte, l'obligation pour les administrateurs de faire partie du bureau\ ?
Est-ce que cela ne risque pas de freiner les propositions d'aide spontanées\ ?
Ceci est inscit dans les statuts de l'association et va donc rester en place pour le moment.
.LI
Le nouveau site peut-il être mis en ligne\ ?
Oui.
Le wiki devra être complété rapidement avec la charte administrateurs et les informations précises pour l'adhésion.
.LE 1
.HU "Référencement des sites Web hébergés"
Faut-il faire un annuaire des sites hébergés par
.B "Ti Nuage" "\ ?"
La décision est prise de le faire sur le Wiki, sa mise à jour étant plus simple par chacun.
Par conséquent, il sera également nécessaire de faire référencer le Wiki afin que les sites puissent profiter des liens publiés.
.HU "Modération des services de partage"
Il s'avère que l'hébergement d'outils de partage anonymes (tels les services
.I Send
et
.I Lutim )
peut poser de très gros problèmes vis à vis de la loi.
Il est envisagé de limiter leur usage aux adhérents.
David doit s'adresser à l'association
.B Infini
pour connaître leur manière de procéder.
L'outil de racourcissement d'URL est quant à lui déjà supprimé.
.HU "Bureau"
L'association n'a plus de secrétaire depuis quelques mois.
Plutôt que de provoquer une Assemblée Extraordinaire, David continue de faire l'essentiel du travail et le poste sera officiellement renouvellé à la prochaine Assemblée Générale.
.HU "Messagerie instantanée"
Nourdine propose d'ouvrir plus largement les canaux de
.I Mattermost
à tout le monde.
David en profite pour suggérer de chercher si une solution différente ne remplirait pas mieux les besoins. Sont évoqués\ :
.DL
.LI
IRC (mais c'est une blague récurrente de David)
.LI
XMPP (ou Jabber)
.LI
Matrix
.LI
Jami
.LE
.P
Il faudrait en débattre encore et tester certains logiciels.
Cette discussion à propos de
.I Mattermost
continuera... sur
.I Mattermost .
.HU "Aide aux utilisateurs"
La question se pose de demander des compensations si beaucoup de demandes nous parviennent de la part d'une structure.
Il est trop tôt pour décider quoi que ce soit, mais les pistes sont nombreuses pour répondre aux besoins qui vont au-delà de la simple administration des services, parmi lesquelles :
.DL
.LI
ouvrir un poste salarié ;
.LI
mettre en relation les structures dans le besoin et des auto-entrepreneurs du Trégor.
.LE
.HU "Canard Réfractaire"
Mathias rapporte que Yohan du
.B "Canard Réfractaire"
(
.URL https://lecanardrefractaire.org
ou
.URL https://nitter.snopyta.org/canard_media
) aimerait nous rencontrer et filmer une personne de l'association.
Nous attendons d'en savoir davantage sur ses souhaits.
Est-ce que cela serait une brève sur
.B "Ti Nuage"
ou un document plus long\ ?
Comme personne ne se montre particulièrement motivé pour se plier à l'exercice, nous proposons de filmer une discussion à plusieurs voix sur un ou plusieurs sujets : le logiciel libre, les CHATONS, notre associations, etc.
Yohan est invité à la prochaine réunion pour que nous disposions de plus d'éléments afin d'y réfléchir.
.HU "Réunion mensuelle"
Mathias revient sur l'idée déjà évoquée ces derniers mois d'organiser un rendez-vous régulier à Vieux-Marché.
Ses idées : un atelier autogéré autour du numérique, de l'association, de la sécurité informatique, sur l'éthique, le matériel, etc.
La fréquence envisagée est d'un rendez-vous par mois.
Le meilleur moment dans la semaine est discuté sans résultat pour le moment.
Le propriétaire du bar de Vieux-Marché sera approché par Vincent L. pour discuter de la possibilité d'occuper la salle de réunion, ou celle du bar.
.P
D'ores et déjà, une prochaine réunion est planifiée.
Ce sera entre le mardi 28 juin et le vendredi 1er juillet.
Les adhérents sont invités sur la liste de diffusion interne à y répondre.
