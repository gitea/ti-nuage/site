.ND 07/03/2024
.TL
Compte-rendu de Réunion
.br
Jeudi 7 mars 2024 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
Cette réunion fut un petit événement dans la vie de l'association, avec dix (10 !) personnes présentes, dont plusieurs membres de l'association Tohu-Bohu, un potentiel nouvel administrateur, une personne ayant envie d'essayer un système libre sur son ordinateur.
Avec pour bilan plein d'idées et d'informations intéressantes, deux nouvelles adhésions et au moins une troisième sans doute pour bientôt...
.AE
.MT 4
.hl
.PRESENT "Benoît, David, Dominique, Félix, Fx, Léna, Mathias, Michelle, Nourdine, Thierry"
.HU "Tohu-bohu et associations voisines"
Une partie du bureau et des animateurs des ateliers numériques de l'association Tohu-Bohu\*F
.FS
.URL https://www.tohu.eu/
.FE
nous a rendu visite.
L'occasion de discuter de la seconde conférence que nous proposerons au café Théodore le 16 mars prochain, à leur invitation, et aussi de faire davantage connaissance de leurs activités et de leurs besoins, à titre collectif ou individuel.
.P
Claire aurait dû être des nôtres ce soir aussi mais a eu un empêchement.
Outre ses besoins personnels d'hébergement Web, elle participe au Kafe\*F
.FS
.URL https://lekafe.fr/
.FE
à Pleumeur-Bodou, espace associatif où sont organisés de manière collégiale des ateliers divers ; réparations, terre cuite...
Elle imagine organiser un week-end autour du numérique, dont la forme et le contenu restent à inventer.
.P
Enfin, Ploum Informatik, mentionnée lors de notre dernière réunion, a été contactée mais n'a pas répondu à notre sollicitation.
.HU "Discussions diverses"
Parmi les sujets abordés, ce compte-rendu doit absolument mentionner le jeu Network & Magic\*F
.FS
.URL https://atelier.aquilenet.fr/projects/communication/wiki/Network_and_Magic
.FE
cité par Fx, nouveau membre enthousiaste déjà très investi dans le libre.
Il s'agit de représenter le parcours d'une requête pour une page Web sur le réseau, chaque joueur interprétant un serveur ; le DNS, Google, Facebook, etc.
Le résultat, très visuel et vivant, est certainement très instructif.
.P
Par ailleurs, les présents ont été invités à questionner le fonctionnement de l'association.
Et ont aussi été abordés l'événement Libre en Fête, le principe du
.I "repair café" ,
les systèmes d'exploitation, les besoins des associations, ... et l'IA.
.HU "Journée d'interconnaissance"
Nous avons été invités à participer à une journée de
.I "rencontre, d'interconnaissance et d'échange entre acteurs du territoire de Lannion-Trégor Communauté" .
Organisée par Lannion-Trégor Communauté en partenariat avec le Hub Bretagne\*F
.FS
.URL https://hub-bretagne.net
.FE
dans le cadre de la démarche PorTReA, laquelle cherche à identifier les besoins et ressources du territoire en matière d'accompagnement au numérique et de mettre en réseau les acteurs locaux.
David ou Nourdine devraient répondre favorablement à l'invitation.
.HU "Accès à Internet"
Nous avions opté il y a un an pour le fournisseur Free, d'une part pour profiter du tarif préférentiel la première année, et d'autre part parce que son offre nous apportait les services réseaux dont nous avions besoin.
L'offre tarifaire est maintenant terminée, et nous envisageons de rejoindre l'association FDN\*F
.FS
.URL https://www.fdn.fr/
.FE
\&.
FDN fournit un accès à Internet nu, mais
.I "propre, neutre et militant" .
David et Nourdine doivent vérifier le coût exact avant très certainement d'adhérer à cette offre.
.HU "Services"
Le logiciel Forgejo\*F
.FS
.URL https://forgejo.org
.FE
a été créé afin de développer une forge similaire à Gitea, que nous utilisons, mais débarrassée de tout contrôle de la part d'une société commerciale.
David propose d'envisager la migration de notre service pour utiliser ce nouvel outil.
Une telle migration est normalement encore assez simple, les deux projets n'ayant pas trop fortement divergé encore.
.HU "Install Party"
Léna nous rendait visite ce soir pour évaluer la possibilité de remplacer le système d'exploitation, et chiffer le disque sur son ordinateur.
Devant l'affluence inattendue, nous avons seulement répondu à quelques questions et l'avons invité à revenir le 24 mars pour le
.I "repair café"
éphémère que propose Nourdine ce jour-là, et qui s'accompagnera d'une
.I "install party"
avec l'aide de membres de l'association.
Cela se déroulera dès 9 heures à la salle Victor Hugo (à la mairie).
.HU "Administration"
Le changement de banque est acté !
Ou presque.
Nous quittons donc le Crédit Coopératif, pour lequel nous avions une préférence mais qui s'est avéré peu... coopératif justement, et très onéreux pour une structure comme la notre.
Nos comptes seront hébergés très bientôt au Crédit Mutuel de Bretagne.
