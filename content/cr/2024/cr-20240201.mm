.ND 01/02/2024
.TL
Compte-rendu de Réunion
.br
Jeudi 1er février 2024 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
Peu d'activité au creux de cet hiver.
Mais Nourdine n'est pas resté inactif depuis le mois dernier, et la séance a été l'occasion de discuter de logiciels de gestion avec Thierry qui cherche une solution libre pour la comptabilité de son association.
.P
Il a aussi été question durant la soirée d'
.I "Intelligene Articifielle" ,
mais le rédacteur de ce compte-rendu ne souhaite pas donner de place ici à un sujet dont les journalistes, lobbyistes et autres zélateurs nous abreuvent déjà quotidiennement (et qui va tout à fait à l'encontre des principes de sobriété énergétique de l'association).
.AE
.MT 4
.hl
.PRESENT "David, Gilles, Nourdine, Thierry"
.HU "Événements"
La conférence proposée au Café Théodore a été bien reçue, malgré un téléscopage malheureux de deux événement le même jour.
Nourdine fait remarquer cependant que nous avons oublié de communiquer à ce sujet sur notre liste de diffusion.
Aïe ! Tâchons de faire mieux la prochaine fois.
Car nous devrions continuer ce partenariat avec une nouvelle intervention à venir.
.P
Gilles nous suggère de contacter aussi
.I "Ploum Informatik" ,
qui propose des séances d'informatique aux personnes qui ont des difficultés avec cet outil.
.P
Nous avons été invités à participer à une journée de débats et d'ateliers autour de la question du numérique au sein des associations à Rennes.
L'invitation est intéressante, mais la distance et d'autres contraintes personnelles font que nous ne participerons probablement pas.
.HU "Matériel"
L'employeur de Nourdine, Mobil Inn\*F
.FS
.URL https://mobil-inn.com
.FE
, offre un espace de stockage à l'association.
Cet espace est maintenant utilisé pour les sauvegardes des machines virtuelles.
Jusqu'ici, seules les données elles-mêmes étaient conservées à l'extérieur de nos murs, chez un prestataire.
Ce deuxième espace conforte donc la redondance de nos sauvegardes, et les complète.
.P
L'association a acquis un nouveau switch (l'équippement qui sert au branchement des réseaux entre nos machines).
Ce
.I "commutateur"
est
.I "managed" ,
ou programmable, ce qui signifie qu'il sera possible de faire une distinction intelligente des données qui transitent entre nos serveurs et, par exemple, d'isoler intelligemment les flux de certaines machines.
Notre précédent
.I "switch"
sera confié à la Hutte.
.P
Enfin, nous avons fait part récemment sur la liste interne d'un problème de
.I "gel"
de notre serveur principal.
Il semble que cela soit dû aux conecteurs NVMe des
.I "disques"
SSD.
Nourdine surveille ce problème, et nous chercherons à les remplacer s'il survient encore.
.HU "Administration"
Le changement de banque envisagé depuis un certain temps progresse, et Nourdine a rendez-vous pour cela avec le Crédit Mutuel de Bretagne d'ici quelques jours.
.P
Nourdine propose aussi la recherche du statut d'intérêt général pour
.B "Ti Nuage" .
Cette reconnaissance permet aux donateurs et membres de l'association bénéficier d'une réduction d’impôt. Les obligations pour l'association seraient alors de procéder chaque année à une déclaration détaillée des montants perçus\*F
.FS
.URL https://www.economie.gouv.fr/cedef/association-reconnue-interet-general
.FE
\&.
