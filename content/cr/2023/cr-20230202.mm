.ND 02/02/2023
.TL
Compte-rendu de Réunion
.br
Jeudi 2 février 2023 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
L'ordre du jour assez consistant a finalement été parcouru assez rapidement, toutes les opérations suivant naturellement leur cours ; la fibre est installée à la Hutte et la recherche de matériel se précise ; tout comme notre participation à la manifestion autour du Numérique à Plougrescant le mois prochain.
.AE
.MT 4
.hl
.PRESENT "David, Nourdine, Isa"
.HU "Analyse de problèmes de couriel"
Isa s'est présentée avec une foule de questions pour la Convergence des Loutres\*F
.FS
.URL "https://www.laconvergencedesloutres.fr"
.FE
et notre rendez-vous mensuel a donc débuté avec une revue de détail des problèmes de courriel.
La réponse magique à nombre de questions a été « il faut essayer Thunderbird\*F
.FS
.URL "https://www.thunderbird.net/fr/"
.FE
», et il est utile de rappeler ici que les logiciels de courriel disponibles sur une interface Web (couramment appelés
.I "Webmail" )
sont
.I "fragiles"
comme toute application Web.
Si la page est rechargée, si elle est restée trop longtemps ouverte et inactive, si un bouton de navgation du navigateur a été utilisé, et dans d'autres cas encore, l'application peut se comporter de manière inappropriée.
.P
L'utilisation d'un
.I "logiciel natif"
(c'est à dire un logiciel qui doit être installé sur votre ordinateur) est préférable lorsque l'utilisation du courriel devient fréquente, et que la taille et la complexité des messages augmente.
Si vous ne l'avez déjà fait, nous vous conseillons donc d'essayer Thunderbird, et le Wiki contient les informations utiles à sa configuration.\*F
.FS
.URL "https://wiki.ti-nuage.fr/fr/Services/Communication/Courriel"
.FE
.P
Nourdine a toutefois noté quelques détails à vérifier ; n'hésitez pas à nous écrire si la solution vous séduit mais que vous rencontrez des problèmes.
.HU "Faites du numérique à Plougrescant"
Nous avons reçu une proposition de plan de salle, et l'affiche est en cours de finition.
.P
Petit passage en revue des actions à mener pour la manifestation qui doit se tenir le 11 mars prochain :
.BL
.LI
David doit encore prendre contact avec le FabLab pour déterminer s'ils possèdent une impression grand format de l'Expo Libre,\*F
.FS
.URL "https://expolibre.org"
.FE
faute de quoi nous prendrions la notre ;
.LI
David doit inscrire notre participation à l'Agenda du Libre dans le cadre de
.I "Libre en fête" ;
.LI
David (décidemment) a entamé le support pour une présentation du logiciel libre, qui donne aussi une liste de logiciels alternatifs pour les usages courants.
Ce support pourra être utilisé pour animer les discussions informelles à Plougrescant, voire servir de guide pour faire un quizz sur les logiciels libres usuels.
Toute aide est la bienvenue pour compléter cette liste de logiciels.
.LE
.HU "Connexion et serveurs à Vieux-Marché"
Le Hutte a été raccordée à la fibre.
Comme développé dans le précédent compte-rendu,\*F
.FS
.URL "https://ti-nuage.fr/cr/2023/cr-20230112.pdf"
.FE
nous avons opté pour l'offre de Free qui possède une exclusivité pour quelques mois avec Orange.
L'accès proposé par Free répond à nos besoins et l'offre, qui s'accompagne d'une promotion, pourra être révisée en fin d'année.
.P
La prochaine étape de notre installation consiste à choisir le matériel à acquérir et à dresser un plan de migration de nos services, ce dernier influant sur les achats ainsi que sur leur échelonnement.
.HU "Avance de fonds"
La seule subvention dont nous disposons actuellement n'étant disponible que sur facture, nous avons besoin d'un prêt (ou d'autres subventions).
L'association
.B "Koolisterik"
(Vieux-Marché) est d'accord pour nous prêter de l'argent jusqu'à 5000€.
Cette offre nous permettrait de disposer d'une avance de fonds pour acheter les premiers éléments matériels de notre installation.
Prudents, Nourdine et David n'envisagent cependant pas de demander plus de 1500€ parce que les modalités et les délais de remboursement du Département ne sont pas connues.
Cette somme permettrait déjà de mettre en place une baie et une première machine.
.HU "Gestion de l'association et du budget"
À propos de budget toujours, et comme abordé dans notre dernier compte-rendu, Roxane Caillon s'occupe maintenant pleinement de la recherce de subventions pour
.B "Ti Nuage" .
Pour mémoire, elle se paie seulement en prélevant un pourcentage sur les sommes obtenues.
Le dossier pour la Fondation Crédit Coopératif est déjà terminé.
.P
En revanche, parce que la banque nous fait des difficultés, Mathias ne pourra assurer les fonctions de trésorier pour lesquelles il s'était proposé.
Ces tracasseries s'ajoutent à d'autres qui nous poussent de plus en plus à chercher un autre établissement bancaire.
.P
.P
Nourdine évalue les logiciels susceptibles de remplacer
.I "dolibarr" ,
l'outil qui sert actuellement à la fois à la gestion de l'association et à celle des adhérents.
Bien que très bien fait, il est un peu complexe pour l'usage limité que nous en avons.
.HU "Nouveaux contacts"
Les Cigales,\*F
.FS
.URL "https://www.cigales-bretagne.org"
.FE
un club d'investisseurs pour aider les association locales, s'est adressé à nous pour trouver une solution de courriels et de listes de diffusion.
Leur besoin est un peu plus complexe que les installations déjà réalisées pour les associations adhérentes à
.B "Ti Nuage" ,
et Nourdine doit discuter avec eux pour mieux cerner encore leurs besoins.
.P
Depuis cette réunion, nous avons aussi reçu des questions de la part d'un petit group de producteurs de fromage et de viande bio du Trégor. Nourdine doit les rencontrer.
.HU "Spicilège"
Nourdine et David ont participé à une réunion d'accueil pour les nouveaux CHATONS.
David s'est engagé à mettre en œuvre ce qu'il faut pour participer au système de statistiques du collectif.\*F
.FS
.URL "https://stats.chatons.org"
.FE
.P
David a commencé à intégrer le calendrier partagé de l'association sur le site.
Les événements à venir dans les trente jours apparaissent dorénavant dans la rubrique
.I "actualités"
de la page d'accueil.
Un calendrier plus complet sera bientôt disponible.
.P
Nous avons observé une occupation surprenante et importante sur le disque des fichiers déposés avec l'outil de partage
.I "Send" .
Après redémarrage du service, cet espace a été... trop bien libéré ; les fichiers normalement encore en cours de partage ont aussi disparu.
Une exploration plus complète du logiciel doit être entreprise pour comprendre son fonctionnement et prévenir tout débordement sur le disque à l'avenir.
