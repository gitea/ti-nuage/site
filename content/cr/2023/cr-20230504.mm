.ND 04/05/2023
.TL
Compte-rendu de Réunion
.br
Jeudi 4 mai 2023 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
Au risque de lasser les lecteurs, cette réunion à l'ordre du jour à peu près vide (le printemps, sans doute) s'est encore concentrée sur des questions matérielles.
Ce court compte-rendu est donc l'occasion de présenter les ressources matérielles pour les personnes intéressées.
.AE
.MT 4
.hl
.PRESENT "David, Nourdine, Vincent"
.HU "Annonces diverses"
Avant de rentrer dans le vif des considérations techniques, David mentionne qu'il a envoyé un courriel à Lannion-Trégor Communauté, sans réponse pour le moment.
(Après vérification pour ce compte-rendu, c'était le 26 avril, à l'adresse de Mme Gaëlle Le Mer.)
.P
David mentionne aussi la manifestation EntreeLibre#3, au Centre des Abeilles à Quimper du 18 au 20 mai.
Un événement fait pour le grand public, avec conférences, ateliers et rencontres diverses.
.HU "Le point sur le matériel"
Cette soirée a donc permis de vérifier une nouvelle fois ce qui a été planifié depuis quelques mois maintenant.
Et après une séance de bricolage imposée à David par Nourdine, les discussions sont allées bon train sur les commande de matériels, l'affectation des ressources, le déroulement de la migration des services...
.P
Reprenons du début : grâce au prêt de l'association
.B "Koolisterik"
(Vieux-Marché), nous avons disposé d'une avance de fonds sur les subventions du département pour pouvoir acheter nos premiers équipements.
Les premiers d'entre eux sont une baie, ainsi qu'un
.I Switch
et un onduleur qui sont installés depuis plusieurs semaines à la Hutte.
Le département s'étant montré assez réactif pour les remboursements, d'autres commandes ont rapidement suivi.
Aujourd'hui, nous disposons des machines nécessaires pour procéder à la migration des services libres, à celle des sites Web des adhérents, à la gestion de nos sauvegardes locales.
Parce qu'ils nécessitent un peu plus d'attention, les services et les données des adhérents seront déplacés dans un second temps.
.P
Nous avons fait le choix d'utiliser autant que possible des machines très légères.
Ce choix s'inscrit dans une démarche économique et écologique (ces machines sont plus simples à produire et plus sobres en terme d'énergie).
Il est aussi cohérent avec la charge modeste que nous avons à supporter aujourd'hui, et suffisamment évolutif pour nous permettre de grossir au rythme observé depuis la création de l'association.
La machine hébergeant les données des adhérents est la seule qui nécessitera vraiment de la puissance.
Elle reste enore à acquérir.
.P
L'association dispose donc des machines suivantes :
.BL
.LI
un micro-PC disposant de plusieurs ports Ethenet pour faire office de routeur.
Qu'elle soit nommée pare-feu, routeur, ou encore
.I front-end
dans le jargon des informaticiens, cette machine a pour fonction de répondre à toute les requêtes entrantes sur le domaine
.I ti-nuage.fr
et à les diriger vers le bon service interne, ou les bloquer au besoin.
Cette machine est en cours d'installation préliminaire à fins de tests.
Nous attendons encore une carte SD pour faire l'installation finale ;
.LI
un micro-PC pour l'hébergement des sites Web des adhérents qui en ont fait la demande.
Il n'est pas besoin de beaucoup de puissance ici, mais de capacités de stockage.
Nourdine a travaillé sur la mise en place logicielle, qui sera plus sécurisée que l'installation actuelle, les données de chaque utilisateur étant compartimentées.
Le disque dur doivent nous parvenir avec la prochaine commande ;
.LI
un micro-PC pour l'hébergement des services libres.
Il s'agit d'une configuration similaire à celle pour l'hébergement des sites des adhérents.
Là aussi, le disque est en attente ;
.LI
une machine pour le stockage des sauvegardes.
L'accent est mis ici sur la rapidité d'écriture des disques afin que le processus de sauvegarde soit aussi rapide que possible.
Une fois encore, un micro-ordinateur suffit à cette fonction qui ne nécessite ni beaucoup de mémoire ni beaucoup de puissance.
.LE
.P
En résumé, et hormis la machine dédiée aux données des adhérents, une grande partie de l'infrastructure des services de l'association est prête à être mise en place.
Et commencera à l'être dès la réception des dernières pièces.
Pour l'heure, une grande difficulté nous occupe encore... Nous mettre d'accord sur le nom à donner à chacune de ces machines !\*F
.FS
Il s'agit des noms internes sur notre réseau.
Invisibles depuis l'extérieur, ces noms sont bien pratiques à chaque fois que l'on a besoin de désigner une machine. (Dire « je mets à jour le système de Dryade » est plus simple et sympathique que : « je mets à jour le système de la machine qui héberge les serveurs Web ».)
.FE
