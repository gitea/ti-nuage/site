.ND 07/09/2023
.TL
Compte-rendu de Réunion
.br
Jeudi 7 septembre 2023 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
Soirée suspense autour du montage et de l'installation du système sur le serveur pour les adhérents... On vous divulgâche la fin : ça fonctionne.
.AE
.MT 4
.hl
.PRESENT "David, Mathias, Nourdine"
.HU "Un point sur le budget"
Nous en avons maintenant fini avec les achats planifiés de matériel et les factures émises auprès du département, dans le cadre de l'appel à projets numériques de l'an passé, ont bien toutes été remboursées.
Nous avons également reçu la subvention de la part du Fonds pour le Développement de la Vie Associative (FDVA)\*F
.FS
.URL https://www.associations.gouv.fr/FDVA.html
.FE
obtenue grâce au travail de Roxane, qui a été payée.
Nous avons aussi remboursé l'avance généreusement octroyée par l'association
.B "Koolisterik"
(Vieux-Marché), qui nous a permis de commencer nos achats avant de recevoir les premiers remboursements du département.
.P
Bref, nous en avons terminé pour un temps avec les mouvements financiers importants et il reste actuellement environ 1600€ sur le compte de l'association.
Nous allons devoir surveiller nos charges dans les mois à venir parce que, d'ici que la migration de tous nos services soit complète, nous aurons des frais un peu plus importants dûs à l'addition de l'électricité, de la fibre, et des locations encore nécessaires chez OVH.
Mais la situation paraît saine.
.HU "Apéro CHATONS"
Mathias propose d'organiser des rencontres entre CHATONS.
Il a listé les structures similaires à la notre, originales dans leur démarche ou proches géographiquement, et envisage de les contacter pour échanger nos expériences.
Rapidement nommés « Apéros CHATONS », les modalités et les occasions de ces rencontres sont encore à définir.
Mathias s'adressera rapidement aux différentes associations proches pour diffuser l'idée.
.HU "Abonnement magazine spécialisé"
Suite à notre abonnement, les derniers volumes des magazines MISC\*F
.FS
.URL https://connect.ed-diamond.com/misc/misc-129
.FE
et Linux Pratique\*F
.FS
.URL https://connect.ed-diamond.com/linux-pratique/lp-139
.FE
ont été reçus.
Ces magazines sont disponibles à la lecture ou à l'emprunt.
D'autres magazines ou d'anciens numéros apportés par les uns ou les autres sont également disponibles à la Hutte.
.HU "Matériel et protocole en cas de pannes"
L'essentiel de la soirée a été consacrée à mettre en service le futur serveur pour les adhérents, lequel nous a donné du fil à retordre.
Le système est toutefois maintenant installé.
.P
Maintenant que la gestion du matériel est totalement à notre charge se pose la question du protocole à appliquer en cas de panné sévère : disque dur disfonctionnel, machine qui refuse de démarrer, etc. Le sujet, tout juste abordé, doit être rapidement discuté... Une autre fois !
