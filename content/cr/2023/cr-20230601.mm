.ND 01/06/2023
.TL
Compte-rendu de Réunion
.br
Jeudi 1er juin 2023 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
Point de progrès dans les opérations de migration ces dernières semaines. Aucune urgence non plus. La réunion a donc fait place à une discussion au sujet des matériels de récupération, des actions menées à droite ou à gauche...
.AE
.MT 4
.hl
.PRESENT "David, Mathias, Nourdine, Vincent"
.HU "État des commandes"
Le compte-rendu précédent était l'occasion de faire un état des lieux sur la migration, avec une courte explication des choix technologiques réalisés. Peu de changements ce mois-ci, du fait de l'attente des derniers remboursements du département. Néanmoins, nous avons choisi le nom des machines et étiquetté ces dernières.
.HU "Organisation"
Sur une idée de Mathias, un message sera maintenant automatiquement envoyé sur la liste de diffusion commune à tous les adhérents afin de rappeler la réunion mensuelle. Ce rappel sera en effet peut-être l'occasion pour certains de se demander s'ils n'ont pas des questions à poser, ou de nous rendre visite pour se tenir au courant de nos activités. Nous avons jugé qu'un message mensuel n'occasionnait pas une grande gêne.
.HU "EntreeLibre #3"
David s'est déplacé à Quimper pour assister à la première journée d'EntreeLibre#3, une rencontre d'artisans du libre à l'attention du public désireux d'en apprendre davantage sur le fonctionnement d'Internet, de l'ordinateur, du mobile...
.P
Petit retour sur les conférences suivies :
.BL
.LI
.I "Au tout début était Internet" ,
par Stéphane Bortzmeyer. Historique, définitions, et exploration d'Internet depuis les services jusqu'au matériel. Stéphane étant du genre bavard, c'est l'horaire imposé par le repas qui aura marqué la fin de son intervention.
.LI
.I "Justice sociale et environnementale" ,
par Maïwann. L'impact du numérique sur l'environnement, sur les services publics, sur notre manière de voir le monde. Une conférence intéressante qui complète la Fresque du numérique\*F
.FS
.URL https://www.fresquedunumerique.org/
.FE
elle-même inspirée de la Fresque du climat\*F.
.FS
.URL https://fresqueduclimat.org/
.FE
.LI
.I "Spam / Phishing" ,
par Natouille. Passage en revue des types de tentatives d'abus aujourd'hui observés, en particulier par SMS, et les conseils pour les repérer et ne pas tomber dans le panneau.
.LI
.I "Tout ça c’est de la science-fiction ?"
par greenman. Un amusant tour d'horizon de quelques œuvres de SF, au regard de notre réalité en matière de robots et d'Intelligences Artificielles en particulier.
.LE
.P
L'après-midi se sont tenus trois ateliers. David a assisté à celui concernant les mobiles (sous Android seulement). Son déroulé est intéressant à noter.
.BL
.LI
Demande des attentes de chacun. Le public, majoritairement issu des personnes qui fréquentent déjà le centre social et ses ateliers, est soit totalement botien, soit en recherche de solutions pour remplacer des applications qu'il ne comprend pas ou qui l'inquiètent.
.LI
Présentation des couches logicielles du mobile. Un mobile sous Android se compose en effet d'AOSP\*F
.FS
.URL https://source.android.com
.FE
(le cœur, libre, du système), d'applications Google obligatoires selon les contrats signés entre Google et les constructeurs, des applications du constructeur lui-même (Samsung, Xiaomi, etc.), et possiblement des applications du fournisseur d'accès internet qui vend le téléphone.
.LI
Prise de conscience de l'existence des pisteurs avec l'installation d'Exodus Privacy. Un concours du plus grand nombre de pisteurs sur une seule application permet de les découvrir un à un, de les regrouper par entreprise et par familles, de se rendre compte des nombreuses entités qui siphonnent les données qui passent par chaque application de manière totalement opaque.
.LI
Installation du magasin d'applications alternatif F-Droid afin d'utiliser des applications alternatives, libres et plus repectueuses. Cette étape, sans être aussi radicale et efficace que l'intervention avec un ordinateur sur le système lui-même (voire le remplacement complet de celui-ci), est accessible à tous et permet de limiter fortement la fuite de données.
