.ND 12/01/2023
.TL
Compte-rendu de Réunion
.br
Jeudi 12 janvier 2023 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
L'ordre du jour est encore une fois conséquent pour cette première réunion (décalée) de 2023.
Pour autant, il n'y avait pas grand monde pour les vœux de la nouvelle année.
Ce compte-rendu est donc une session de rattrapage pour toutes les bonnes volontés ; nous avons beaucoup d'actions à mener dans les mois à venir.
.AE
.MT 4
.hl
.PRESENT "David, Nourdine"
.HU "CHATONS"
Il n'y avait guère de suspense, mais cela fait tout de même extrêmement plaisir :
.B "Ti Nuage"
fait maintenant partie des CHATONS.\*F
.FS
Le Collectif des Hébergeurs Alternatifs, Transparents, Neutres et Solidaires\ :
.URL https://www.chatons.org
.FE
Le site a déjà été modifié pour signifier que nous ne sommes plus seulement candidats, et Nourdine et David devraient assister à une réunion de bienvenue le 23 janvier.
.HU "Faites du numérique à Plougrescant le 11 mars 2023"
Cette manifestation, proposée par Grégoire (adhérent à
.B "Ti Nuage"
et élu à Plougrescant), a déjà été présentée dans le compte-rendu du mois dernier.\*F
.FS
.URL "https://ti-nuage.fr/cr/2022/cr-20221201.pdf"
.FE
David et Mathias ont depuis participé à une réunion permettant de mieux la définir.
Devraient être présents le Fab-Lab de Lannion, l'association
.B "Promouvoir et Agir en Trégor Gouelo"
(PATG), des représentants du département et de Plougrescant et quelques autres encore.
Sont envisagés des ateliers ou des rencontres autour du smartphone, de la photographie numérique, de la musique assistée par ordinateur (MAO), de la réalité virtuelle (RV ou VR pour faire bien), des machines de prototypage rapide, la présence d'un robot Pepper (ou Nao), etc.
.P
.B "Ti Nuage"
sera présent en particulier pour discuter de la notion de
.I "cloud souverain" .
Nous envisageons pour cette occasion d'apporter l'Expo Libre\*F
.FS
.URL "https://expolibre.org"
.FE
que nous possédons sous la forme de panneaux au format A3 (mais peut-être le Fab-Lab pourra-t-il nous prêter ses propres impressions en grand format).
Nous devons contacter les associations dont nous sommes proches (April, FramaSoft, ...) pour disposer de quelques flyers et autocollants.
Nous présenterons évidemment nos services, et discuterons plus généralement des questions relatives à notre domaine de compétence ; la notion de vie privée, l'usage des données, les coûts énergétiques du numérique.
.P
Nous envisageons aussi de proposer de manière informelle une sorte de quizz pour ouvrir la discussion sur les logiciels alternatifs et libres.
David doit préparer le support, mais toutes les bonnes volontés sont les bienvenues pour apporter du contenu ; quels sont les logiciels que vous utilisez, dans quels domaine d'activité, et quels logiciels privateurs remplacent-ils ?
.P
Enfin, nous réfléchissons aussi à la manière de montrer au public de manière claire ce que sont les conditions d'utilisation des services les plus utilisés (Instagram, Snapchat, ce genre de choses).
Nourdine va commencer à travailler sur l'assemblage de ces documents. Il aura aussi très probablement besoin d'aide pour ne pas finir asphyxié sous les masses de documents !
.HU "Éducation au numérique"
Nourdine doit recontacter l'équipe administrative du collège de Plouaret pour mieux définir ce qu'ils souhaitent pouvoir proposer aux élèves.
Les documents déjà à l'étude ou en cours de rédaction pour la manifestation à Plougrescant devraient pouvoir être ré-employés pour cela.
.HU "Quinzaine de la parentalité"
Il ne semble plus y avoir d'activité du côté des organisateurs.
Nous nous concentrons sur nos autres projets, et nous attendons d'être sollicités plus clairement si quelque chose doit finalement se produire.
.HU "Connexion et serveurs à Vieux-Marché"
La fibre est disponible à Vieux-Marché comme attendu.
Tandis que certains étaient à Plougrescant, Nourdine faisait le point avec
.B "Les castors perchés"
pour définir les modalités de notre installation et du partage de la connexion.
Il a été décidé que la gestion et les frais de la connexion étaient laissés entièrement aux soins de
.B "Ti Nuage" ,
qui la partagerait avec la Hutte.
En contrepartie, nos serveurs seront hébergés gracieusement, et seule la consommation électrique nous sera facturée.
.P
Nourdine a étudié la disponibilité de la fibre selon les opérateurs, ainsi que les tarifs et les propositions techniques de ceux-ci.
Pour des questions d'éthique, d'indépendance et de facilités techniques, nous penchons à terme vers une offre telle que celle de la FDN.\*F
.FS
French Data Network\ :
.URL "https://www.fdn.fr"
.FE
Cependant, une exclusivité pour Orange et Free existe pour une période indéterminée sur les installations dans le secteur.
Free présente l'avantage de fournir assez clairement toutes les solutions techniques pour nos besoins.
Nous envisageons donc d'opter pour l'offre d'engagement sur un an de cet opérateur, à un tarif avantageux, et de refaire une étude d'ici la fin de l'année.
.P
Dans le même temps, et afin de nous assurer que la solution d'hébergement à la Hutte est viable, Nourdine et David ont profité de la réunion pour mesurer précisémment le local envisagé, et doivent maintenant passer en revue les formats de baie et de serveurs existants.
Ceci permettra aussi d'avancer dans le choix du matériel et de consolider le coût de la migration des services et des données.
.HU "Subventions"
Suite à nos discussions lors de la dernière réunion, Grégoire a avancé notre nom au sein des instances de Lannion-Trégor-Communauté et nous devrions être contactés bientôt afin de mieux nous présenter et de nous diriger vers le bon guichet pour demander des aides.
.P
Par ailleurs, Nourdine a contacté Roxane Caillon comme nous l'avions décidé, afin qu'elle prenne en charge notre recherche de subventions.
Pour mémoire, elle propose la recherche d'aides institutionnelles ou autres ainsi que leur suivi.
Elle se rémunère sur les aides obtenues.
Un dossier pour le Crédit Coopératif est déjà en cours de réalisation.
.HU "Miscellanées"
L'association devrait prochainement faire un communiqué afin de faire connaître son appartenance aux CHATONS. David suggère que ce comuniqué soit retardé à la fin du mois de février afin de pouvoir y inclure des détails sur l'arrivée de la fibre et notre migration de données, ainsi que pour annoncer dans un temps approprié notre participation à l'événement de Plougrescant.
.P
Nourdine a installé un nouveau logiciel client de courriel au sein de
.B "Next Cloud" .
Cette proposition est pour le moment complémentaire au logiciel
.B "Roundcube"
déjà disponibe dans le SSO, et le Wiki a été mis à jour pour décrire l'étape de configuration nécessaire pour l'utiliser.
