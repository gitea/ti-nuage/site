.ND 05/10/2023
.TL
Compte-rendu de Réunion
.br
Jeudi 5 octobre 2023 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
Les plans pour relocaliser les services se précisent. Les discussions ont couru sur les événements récents dans la région (les activités d'Infothema ou du FabLab de Lannion). Une réunion assez courte et efficace de plus.
.AE
.MT 4
.hl
.PRESENT "David, Mathias, Nourdine"
.HU "Consommation électrique"
Nourdine a commencé les relevés sur la prise électrique qui alimente notre baie.
Pour mémoire, l'association doit à la Hutte, qui nous héberge, les frais d'électricité relatifs au matériel réseau et aux serveurs.
D'après les premières estimations, nous sommes largement à l'intérieur des projections que nous avions réalisées.
Nous pensons nous acquitter de la facture tous les trois ou six mois.
.HU "Plan de panne"
Dès que les services les plus importants seront servis par nos propres machines, il faudra être en mesure de fournir une solution de repli en cas de panne.
Nous n'avons pas encore aujourd'hui de solution définitive, et nous explorons ce qu'il est possible de faire.
.P
La virtualisation et la conteneurisation de chacun de nos serveurs nous permet d'envisager une relocalisation rapide des services.
C'est à dire en quelques heures, selon nos disponibilités, car comme il est écrit dans nos Conditions d'utilisation\*F
.FS
.URL https://www.ti-nuage.fr/cgu.html
.FE
:
.I "Ces services [...] reposent essentiellement sur le bénévolat d’une poignée d’administrateurs, sur un modèle de best effort en cas d’incident par exemple, sans garantie de continuité de services ou autre" .
.P
Par panne, nous entendons ici une sérieuse avarie matérielle.
Selon que la panne survient sur une machine ou plus largement sur la baie, nous étudions :
.BL
.LI
le coût nécessaire pour acheter une machine supplémentaire, capable d'accueillir temporairement les services les plus cruciaux pour les utilisateurs (transport du courriel, accès aux fichiers stockés, disponibilité des sites Web) ;
.LI
la possibilité de louer un VPS (serveur virtuel) chez un hébergeur comme OVH pour déployer ces services le temps de pouvoir réparer ou remplacer le matériel défectueux.
.LE
.HU "Les cigales de Bretagne"
Nous recevons ces derniers temps quelques nouvelles demandes d'adhésion, et certaines requièrent des services ou des configurations spécifiques.
C'est le cas des Cigales de Bretagne\*F
.FS
.URL https://www.cigales-bretagne.org/
.FE
qui sont en discussion depuis plusieurs mois avec nous.
.P
Les Cigales sont une grosse structure et leurs besoins sont complexes : une boîte de courriel principale, une boîte pour chacune des cigales, des dizaines de listes de diffusion et du stockage de fichier.
Nourdine envisage la possibilité d'installer une machine virtuelle spécialement pour eux.
Cette solution est facile à mettre en œuvre sur notre nouveau serveur et permettrait d'une part une isolation de leurs données, et d'autre part une certaine lattitude pour eux dans l'administration de leurs services.
.HU "Achat de consommables"
Pour en finir avec ce compte-rendu, il faut rapporter que notre président a décidé que l'association procéderait à quelques achats pour pouvoir boire ou grignotter pendant les prochaines réunions.
Nous nous sommes évidemment bien gardés d'émettre un avis contraire.
