.ND 02/03/2023
.TL
Compte-rendu de Réunion
.br
Jeudi 2 mars 2023 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
L'ordre du jour assez consistant a finalement été parcouru rapidement, toutes les opérations suivant naturellement leur cours. Les sujets abordés sont principalement la recherche de matériel pour notre installation imminente à la Hutte et notre participation à la manifestion autour du Numérique à Plougrescant le mois prochain. Le tout dans une ambiance agréable d'auberge espagnole autour des victuailles apportées par chacun.
.AE
.MT 4
.hl
.PRESENT "Ania, David, Mathis, Nourdine"
.HU "Faites du numérique à Plougrescant"
Les actions réalisées ou prévues sont les suivantes.
.BL
.LI
David a envoyé un communiqué de presse et posté un billet sur Mastodon pour signifier notre participation à la manifestation de Plougrescant.
.LI
Notre participation a aussi été inscrite sur l'Agenda du Libre\*F
.FS
.URL "https://www.agendadulibre.org"
.FE
dans le cadre de
.I "Libre en fête" ,
puisque parlerons de logiciels libres ;
.LI
Le FabLab nous prête l'Expo Libre\*F
.FS
.URL "https://expolibre.org"
.FE
dont ils disposent en grand format. Florian Vasseur l'apportera sur place, et nous avons normalement des grilles pour l'accrocher.
.LE
.P
Enfin, David a apporté le support envisagé pour présenter le logiciel libre et faire un tour d'horizon des logiciels alternatifs existant pour les usages courants ; bureautique, loisirs, médias... Une partie de la soirée devient l'occasion de projeter cette présentation et de faire un
.I brainstorming
pour compléter les logiciels méritant d'être évoqués.
.HU "Éducation populaire"
À propos de présentations, David a approché l'association
.I "Promouvoir et Agir dans le Trégor"
(PATG) qui donne des cours d'informatique en divers endroits et notamment à Lannion. Il s'avère que l'activité est encore balbutiante et les élèves peu assidus. Mais les connaissances que nous pouvons apporter les intéresse et nous pourrons faire un point à la rentrée, après la tenue des différents forums aux associations.
.HU "Achat de matériel informatique"
Le reste de la soirée a consisté en une discussion sur le matériel à acheter et les priorités à donner sur la migration des différents. Sous peu, une baie devrait prendre place à la Hutte, ainsi qu'un switch et un onduleur. La réflexion continue pour le choix des machines...
.P
Et ces points de suspension me permettent de terminer au bas d'une seule page, pour le compte-rendu le plus succinct de la courte histoire de l'association !
