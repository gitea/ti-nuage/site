.ND 07/12/2023
.TL
Compte-rendu de Réunion
.br
Jeudi 7 décembre 2023 à Vieux-Marché
.AU "Nourdine"
.MT 4
.hl
.PRESENT "Gilles, Mathias, Nourdine"
.PRESENCE "Excusé(e)s" "David, Julie (Joyeux anniversaire !), Vincent (parce que c'est l'anniversaire de Julie)"
.HU "Questions posées sur le pad"
.B "Julie :"
Est-ce que le nouveau boulot de Nourdine interroge sa disponibilité pour s'occuper de
.B "Ti Nuage"
et donc remet en question le fonctionnement actuel ?
.P
.B "Nourdine :"
Cette situation ne remet pas du tout en question le fonctionnement de
.B "Ti Nuage" ,
au contraire elle m'apporte une montée en compétence et me permet de renforcer la configuration actuelle.
Elle me demande de répondre à une problématique d'entreprise qui nécessite de la rigueur, rigueur qu'il est facile de ne pas atteindre dans le milieu bénévole.
En bref, ce boulot me permet d'affiner la configuration des serveurs de l'association à un point que je n'aurais peut-être pas atteint (ou très lentement) si je n'avais pas à le développer en milieu professionnel.
Et, niveau disponibilité, c'est un temps partiel, ce qui me laisse tout le loisir de continuer à m'occuper de
.B "Ti Nuage"
sans concessions.
.sp
.B "Julie :"
Comment ça se passe au niveau du nombre d'utilisateurs pour la bonne marche de l'association, en terme humain et financier et/ou bonne marche du serveur ?
A-t-on suffisamment d'utilisateur·rices ?
En faudrait-il un peu plus, un peu moins, ou n'est-ce pas en ces termes que la question se pose ?
.P
.B "Nourdine :"
Aujourd'hui, avec environ 45 adhérents, nous sommes à flot entre les revenus et les dépenses.
Avoir plus d'adhérents n'est pas une priorité mais cela permettrait :
.BL
.LI
de réduire la cotisation annuelle ; avec plus adhérents les frais pour l'association n'augmenteraient pas, ou de manière insignifiante ;
.LI
de mettre de l'argent de côté afin de prévenir l'achat de matériel, suite à une panne par exemple.
.LE
.HU "Système de sauvegarde"
Le serveur
.I "yosei"
(l’hyperviseur Proxmox\*F
.FS
.URL "https://www.proxmox.com/en/proxmox-virtual-environment/overview"
.FE
) sauvegarde les VM (ou serveurs virtuels) par
.I "snapshot" .
Un
.I "snapshot"
a aujourd’hui une taille d'environ 80Go pour la VM principale, et est envoyé vers le serveur local
.I "doppleganger"
chaque nuit.
Plus il y aura d'adhérents et de données, plus cette taille augmentera et un
.I "snapshot"
complet sera de plus en plus long à sauvegarder (environ quinze minutes actuellement).
La solution à moyen ou long terme sera de trouver la manière d'installer sur une machine la solution Proxmox Backup Server\*F
.FS
.URL "https://www.proxmox.com/en/proxmox-backup-server/overview"
.FE
qui permet de faire des sauvegardes incrémentielles.
.HU "Hardware Yosei"
Lors de l'achat de
.I "yosei" ,
la configuration matérielle à été choisie pour ne pas dépasser l'enveloppe de la subvention et une concession à été faite sur la RAM.
Le serveur dispose de 16Go et en tant qu'hyperviseur c'est vite limitant.
Maintenant que les finances le permettent, il est prévu d'acheter 64 ou 96Go de RAM supplémentaire à installer sur cette machine.

.HU "Code bash et ChatGPT"
Nous avons découvert comment ChatGPT\*F
.FS
.URL "https://fr.wikipedia.org/wiki/ChatGPT"
.FE
s'avère être un outil puissant comme assistant codeur.
Il permet de corriger du code et aussi de découvrir des fonctions en lui demandant simplement quel code pourrait avoir tel effet.
Mais comme nous l'a fait remarqué David (qui était absent mais avec qui nous avons pu échanger rapidement sur le sujet), ChatGPT est un outil qui pollue beaucoup et son impact environnemental est aussi important que ses performances\*F
.FS
Lequel David profite de sa participation à la mise en forme de ce compte-rendu pour préciser qu'il aurait encore bien des choses à ajouter au sujet de ce genre d'outils, et qu'il ne participera pas à son usage.
.FE
\&. ChatGPT consommerait 1 litre d'eau pour 100 requêtes\*F
.FS
.URL "https://www.presse-citron.net/chatgpt-nest-pas-alcoolique-mais-il-a-un-vrai-soucis-avec-leau/"
.FE
\&.
Nourdine se renseigne sur la faisabilité d'installer le modèle de langage llama2\*F
.FS
.URL "https://ai.meta.com/llama/"
.FE
sur une VM de l'association.
