---
permalink: false
title: Les services fournis par Ti Nuage
---
Les serveurs fournissant les services ci-dessous sont sous le seul contrôle de
l'équipe technique de l'association.

Nous sommes une petite structure et nous nous efforçons de donner le meilleur de nous même avec les moyens que nous avons à notre disposition. Malgré des sauvegardes quotidiennes et un suivi régulier de l'infrastructure, nous ne sommes pas à l'abris d'un incident. Nous ne garantissons donc pas une continuité de service permanente et nous ne nous assignons aucune obligation de résultat.
