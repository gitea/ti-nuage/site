---
title: Compte-rendu de la réunion 3 novembre
date: 2022-11-06
author: David Soulayrol
---
Le compte-rendu de la dernière permanence de l'association à la Hutte à Vieux-Marché est [disponible ici](/cr/2022/cr-20221103.pdf). Du fait des vacances et des impératifs de chacun, la réunion s'est réduite à un duo, faisant de ce compte-rendu un record de concision.

Le prochain <em>premier jeudi</em> aura lieu le <b>1er décembre</b>, toujours à la Hutte. Vous pouvez d'ores et déjà indiquer votre présence, proposer des sujets à aborder ou poser vos questions sur [ce pad](https://apps.ti-nuage.fr/pad/p/2022-11-reunion-mensuelle).
