---
title: Réunion du mois d'août
date: 2022-07-22
author: David Soulayrol
---
La prochaine réunion de l'association se tiendra le **jeudi 4 août** à partir de 20 heures à... [La Hutte](https://lahutte.ti-nuage.fr). En effet, l'association *Les castors perchés* a décidé de mettre gracieusement à disposition son espace de travail pour les réunions des associations menées par des bénévoles, ce qui est notre cas. Cet espace convivial comprend un coin cuisine, aussi n'hésitez pas à apporter un peu de thé, de tisane, ou une bouteille de bière ou de cidre pour agrémenter nos rencontres et participer aux ressources communes.

Cette réunion marque également notre volonté de rendre nos rendez-vous mensuels plus réguliers et facilement prévisibles. Suite au sondage proposé aux adhérents, nous avons choisi *le premier jeudi de chaque mois*. Vous pouvez donc d'ores et déjà annoter vos agenda pour les semaines à venir.

Comme toujours, cette réunion est ouverte à tous. Nous sommes là pour répondre à vos questions sur les outils et les usages numériques. Il sera également question cette fois-ci :

- de l'état de notre candidature au collectif [CHATONS](https://chatons.org) ;
- de notre réponse à l'appel à projets numériques du département ;
- de la Fête des Possibles qui se déroulera en septembre.

Nos plans pour héberger nos serveurs à Vieux-Marché progressent également. Nous envisageons aussi de collaborer encore plus étroitement avec la Hutte. Venez nous voir pour en apprendre davantage !
