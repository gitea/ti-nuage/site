---
title: Le billet de décembre
date: 2022-12-12
author: David Soulayrol
---
Voici, avec un peu plus de délai que d'habitude, le compte-rendu de notre dernier « premier jeudi du mois ». Il est [disponible à cette adresse](/cr/2022/cr-20221201.pdf).

Comme vous pourrez le lire, l'association possède maintenant les moyens de concrétiser l'implantation de ses outils à Vieux-Marché. Elle est également sollicitée pour différentes rencontres. Et certains de ses adhérents sont porteurs d'idées très intéressantes. Tout ceci nécessite maintenant un peu d'investissement humain. Aussi, si vous aimez aller à la rencontre des gens pour discuter de services numériques et de vie privée, si vous avez envie de vous frotter à un peu de programmation (très simple !), si vous voulez aider à tester des outils, n'hésitez pas à vous manifester.

Cela dit, l'année s'achève et nous vous souhaitons de bonnes fêtes. Nous vous reverrons bien sûr dès le mois prochain, **mais ATTENTION** : notre prochain rendez-vous sera exceptionnellement décalé au *deuxième jeudi* de janvier. En effet, une partie des membres de l'association sera le 5 janvier à Plogrescant pour préparer la manifestation *Faites du numérique* qui s'y tiendra au mois de mars. Nous serons donc plus facilement tous présents le **12 janvier** à la Hutte. Vous pouvez d'ores et déjà indiquer votre présence, proposer des sujets à aborder ou poser vos questions sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-01-reunion-mensuelle).
