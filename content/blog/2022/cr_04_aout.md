---
title: Compte-rendu de la réunion du 4 août
date: 2022-08-08
author: David Soulayrol
---
Ce 4 août s'est tenu notre premier « premier jeudi du mois », la date maintenant fixée pour nos rendez-vous mensuels. La soirée s'est déroulée en petit comité et a été studieuse. Le compte-rendu  est [disponible ici](/cr/2022/cr-20220804.pdf).

*Addendum* au compte-rendu : nous remercions [les castors perchés](https://lahutte.ti-nuage.fr) de nous donner accès à la Hutte, l'espace de travail partagé du Vieux-Marché. Si vous avez besoin d'une salle pour une réunion, une demi-journée de travail ou davantage, n'hésitez pas à faire vivre ce lieu.
