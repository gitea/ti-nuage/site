---
title: Célébrations, lectures et travaux
date: 2023-08-19
author: David Soulayrol
---

## Joyeux anniversaire Debian

[Debian](https://fr.wikipedia.org/wiki/Debian) est une distribution Linux, c'est à dire un système d'exploitation construit autour du [noyau Linux](https://fr.wikipedia.org/wiki/Linux), accompagné d'un ensemble de logiciels libres (plus de 20000) testés et maintenus pour plusieurs types de machines. Mais Debian c'est aussi une communauté importante, une structure démocratique transparente, et un [contrat social](https://www.debian.org/social_contract) qui souligne l'importance du logiciel libre.

Pourquoi on en cause aujourd'hui ? Parce que Debian est la distribution de choix des administrateurs de **Ti Nuage**, et que le projet vient tout juste de souffler [ses 30 bougies](https://bits.debian.org/2023/08/debian-turns-30-fr.html) !

## Alternatives numériques

L'été est l'occasion de découvertes sympathiques, et pas seulement dans l'assiette ou la paysage. Ainsi, ce chouette billet sur [une sélection de vendeurs](https://alternatives-numeriques.fr/ou-trouver-des-pc-et-des-smartphones-libres/) pour acheter du matériel en vue d'installer un système libre, qui vient de paraître et qui fait le point sur quelques constructeurs ou assembleurs qu'il est intéressant de connaître lorsque l'on recherche une nouvelle machine. De plus l'article a le bon goût de proposer la possibilité de la location, et de rappeler que de nombreux systèmes libres peuvent très bien fonctionner sur un ordinateur que vous pensez obsolète.

L'article est paru sur un média que nous découvrons par la même occasion et qui mérite d'être connu : [Alternative Numériques](https://alternatives-numeriques.fr).

## Relocalisation de nos services

Dans le cadre de la relocalisation de nos services, plusieurs d'entre eux ont enfin acquis leur propre sous-domaine. Un sous-domaine, c'est une étiquette placée devant le nom du domaine. Par exemple, dans **wiki.ti-nuage.fr**, **ti-nuage.fr** est ce que l'on appelle communément le domaine, et **wiki** en est un sous-domaine. L'intérêt est que la hiérarchie à l'intérieur du domaine de **Ti Nuage** s'en trouve simplifiée et que, pour qui n'utilise pas son espace personnel pour les retrouver, il est plus facile de se remémorer l'adresse d'un service pour la saisir dans son navigateur. Ainsi :

- l'outil de sondage est maintenant disponible à l'adresse [https://date.ti-nuage.fr](https://date.ti-nuage.fr) ;
- l'adresse du service de partage d'extraits de texte est maintenant [https://bin.ti-nuage.fr](https://bin.ti-nuage.fr) ;
- Wallabag, le logiciel pour stocker les articles à lire plus tard, est maintenant disponible à l'adresse [https://wallabag.ti-nuage.fr](https://wallabag.ti-nuage.fr) ;
- TTRSS, l'agragateur pour les articles et les actualités, se trouve maintenant à l'adresse [https://ttrss.ti-nuage.fr](https://ttrss.ti-nuage.fr) ;
- et l'adresse de la forge logicielle est maintenant [https://forge.ti-nuage.fr/gitea](https://forge.ti-nuage.fr/gitea).

Par ailleurs, une partie des logiciels proposés par **Ti Nuage** est d'ores et déjà installée sur nos propres machines, à Vieux-Marché. Félicitations à Nourdine qui s'arrache encore un peu les cheveux sur un problème ici ou là, mais qui travaille à porter ainsi les services en libre accès de l'association. Ont donc déjà migré :

- [EtherCalc](https://calc.ti-nuage.fr/), le tableur qui permet l'édition collaborative simultanée ;
- [Lufi](https://send.ti-nuage.fr/), qui remplace FireFox Send, utilisé jusqu'ici, pour partager des fichiers ;
- [OpenSondage](https://date.ti-nuage.fr/) et [PrivateBin](https://bin.ti-nuage.fr/), cités plus haut.

Et dans le mouvement, un nouveau service permettant d'éditer et de manipuler les fichiers PDF a été installé : [Signaturepdf](https://pdf.ti-nuage.fr/). Ce logiciel a été écrit par [24ème](https://www.24eme.fr/), un membre du réseau [Libre Entreprise](https://www.libre-entreprise.org) qui réunit des enterprises à structure horizontale et qui œuvrent pour le logiciel libre.

Tout ceci va occasionner des incohérences et peut-être quelques liens cassés dans les pages du site. Ces désagréments vont rentrer dans l'ordre petit à petit. Et si vous rencontrez un problème sur un service ou l'autre, n'hésitez pas à nous contacter, ou à venir nous voir...


## Réunions de l'association

Point de compte-rendu ce mois-ci, la dernière rencontre n'ayant réuni qu'une paire de membres du bureau qui ont discuté technique en buvant du thé. Mais les réunions mensuelles continuent bien sûr, et la prochaine se tiendra le **7 septembre**, toujours à la Hutte. L'ordre du jour se construit sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-09-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir aborder.
