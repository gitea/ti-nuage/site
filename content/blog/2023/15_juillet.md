---
title: Rencontres estivales
date: 2023-07-21
author: David Soulayrol
---

Ce mardi 18 juillet avait lieu à la brasserie Kerampont une conférence pour tout public sur les « impacts sociétaux du numérique ainsi que quelques outils pour aborder une vie numérique plus paisible ». La conférence était proposée par Killian, le représentant dans le Trégor du [RésiLien](https://resilien.fr), un CHATONS présent à la fois à Lannion et dans la Loire.

Le support de la conférence sera mis à disposition bientôt sur le site du RésiLien, mais il est déjà possible de consulter sur son blog [cet article](https://resilien.fr/blog/limpact-environnemental-du-num%C3%A9rique) sur les effets des activités numériques sur l'environnement, ou [cet autre document](https://resilien.fr/blog/la-sobri%C3%A9t%C3%A9-num%C3%A9rique) qui donne une introduction à la sobriété numérique.

Cette conférence fut l'occasion de faire un peu plus connaissance avec Killian, déjà rencontré lors d'une manifestation autour du Logiciel Libre au FabLab l'an dernier. Elle fut aussi l'occasion de faire connaissance avec la présidente de [l'April](https://www.april.org/) (qui a d'excellents goûts puisqu'elle est en vacances dans la région !), laquelle sera également présente [ce samedi à Bégard](https://www.agendadulibre.org/events/27786) pour une présentation de l'April et une rencontre avec les libristes du Trégor.

## Réunions de l'association

Le compte-rendu de notre dernier premier jeudi est [disponible ici](/cr/2023/cr-20230706.pdf). La soirée s'est déroulée en comité très privé et de manière très studieuse. Outre un état des lieux sur nos finances, nous avons progressé sur l'installation de notre routeur et sur l'hébergement des sites Web des adhérents.

Notre prochaine réunion, pleinement estivale, se tiendra un jeudi, évidemment, et plus précisément le **3 août**, toujours à la Hutte. L'ordre du jour se construit sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-08-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir aborder.
