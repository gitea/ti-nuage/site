---
title: Assemblée Générale et vents violents
date: 2023-11-20
author: David Soulayrol
---

## Assemblée Générale

Le compte-rendu de l'Assemblée Générale qui s'est déroulée le 16 novembre à la Hutte à Vieux-Marché est [disponible ici](/cr/2023/cr-ag-20231116.pdf). Peu d'adhérents se sont montrés cette année, mais nous avons eu le plaisir de voir le président d'Infothema (Bégard) avec qui il est toujours agréable de discuter pour partager points de vue et expérience sur la sensibilisation au logiciel libre. Nous avons aussi eu l'heureuse surprise de discuter avec une représentante de Ouest-France qui avait fait le déplacement.

## Retour sur le tempête

La dernière tempête a durement touché le département, et nos adhérents ont donc pu découvrir l'aspect le moins agréable de la décentralisation des services hors des gros centres de données ; l'indisponibilité. (Tout au moins pour ceux qui avaient encore de quoi allumer ordinateurs et smartphones pour pouvoir observer le problème.)

L'interruption des services a duré environ trois jours, le temps que l'életricité soit rétablie de façon suffisament fiable. Toutes les données ont bien sûr été préservées, et nous ne déplorons aucun effet secondaire. Nous n'avons pas mis en place de solution temporaire cette fois, jugeant que la gêne était assez restreinte au regard de la situation. Si vous voulez discuter à ce propos, nous accueillons avec plaisir lors des prochaines réunions.

## Prochains jeudis

Et justement, passées les intempéries et la réunion particulière de ce mois, nous reprenons les rendez-vous mensuels, chaque premier jeudi du mois, pour la fin de cette année et le début de la prochaine. Notre prochaine rencontre aura donc lieu le **7 décembre**, toujours à la Hutte. L'ordre du jour se construit sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-11-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir aborder.
