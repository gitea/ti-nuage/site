---
title: Une année de plus
date: 2023-12-10
author: David Soulayrol
---

## Appel à projets numériques

Le Conseil Départemental des Côtes d'Armor lançait au début de l'été un appel à projets numériques. C'est ce même appel qui a valu en 2022 à notre association d'obtenir une subvention, laquelle lui a permis de s'équiper plus rapidement que nous aurions pensé. Les nouveaux projets présentés ressemblent dans leur diversité à ceux de l'an passé. Nous notons parmi eux celui de [Esprit FabLab](https://esprit-fablab.org), le FabLab de Rostronen, qui souhaite développer des services numériques à la manière des [CHATONS](https://www.chatons.org). Nous leur souhaitons bon courage et espérons les revoir bientôt.

## Visite au café Théodore

Un groupe de personnes de [l'association Tohu-Bohu](https://www.cafetheodore.fr/tohu-bohu/) organise des ateliers numériques au [café Théodore](http://www.cafetheodore.fr). Ce 25 novembre, ils proposaient une première conférence avec pour sujets le courriel et le « cloud ». Nous avons fait le déplacement et rencontré une poignée d'animateurs sympathiques et investis, ainsi qu'un public qui l'était tout autant. Peut-être pourrons-nous nous impliquer dans des conférénces futures.

## Ada et Zangemann

C&F Éditions publie depuis peu un [album jeunesse](https://cfeditions.com/ada/) intitulé *Ada & Zangemann, un conte sur les logiciels, le skateboard et la glace à la framboise*. Il s'agit d'un ouvrage libre a plus d'un titre :

- il raconte de façon enthousiasmante la ré-appropriation par des enfants des appareils numériques qui les entourent ;
- il est diffusé selon la licence Creative Commons : Attribution, Partage dans les mêmes conditions (CC BY-SA 4.0 FR) ;
- sa version française est le fruit d'une traduction par plus d’une centaine d’élèves d’Alès, Besançon, Guingamp et Paris, proposée par [Alexis Kauffmann](https://fr.wikipedia.org/wiki/Alexis_Kauffmann) (fondateur de [Framasoft](https://framasoft.org/fr/)) et coordonnée par [l'ADEAF](https://adeaf.net) sur l’outil libre Digipad de [La Digitale](https://ladigitale.dev) (le processus est à lire dans [cette synthèse](https://cfeditions.com/ada/ressources/Bulletin_ADEAF_n_153_A-Z_48-54.pdf));
- il est édité avec [Paged.js](https://pagedjs.org), un logiciel libre permettant de formatter un document HTML pour l'impression.

Disponible en papier donc, mais aussi en version PDF à prix libre. La lecture est tout aussi plaisante pour un adulte et, pour finir de vous convaincre, le livre — soutenu par la [Free Software Foundation Europe (FSFE)](https://fsfe.org/activities/ada-zangemann/) — est accompagné de jolis autocollants.

## Prochains jeudis

Le compte-rendu du dernier jeudi de l'association de l'année, qui s'est déroulée ce 7 décembre, est [disponible ici](/cr/2023/cr-20231207.pdf). Une fois n'est pas coutume, il est de la main de Nourdine.

Notre prochaine rencontre aura lieu le **4 janvier**, à la Hutte bien sûr. L'ordre du jour se construit sur [ce pad](https://apps.ti-nuage.fr/pad/p/2024-01-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir aborder.

Passez de bonnes fêtes de fin d'année !
