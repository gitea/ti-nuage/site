---
title: Le billet de février
date: 2023-02-12
author: David Soulayrol
---

## Les Castors Perchés

Ce mercredi 15 février à 18 heures se tient l'Assemblée Générale de l'association **Les Castors Perchés** à [La Hutte](https://lahutte.ti-nuage.fr) au Vieux-Marché. Il s'agit de l'association qui gère cet espace partagé *ouvert aux entrepreneurs, aux télétravailleurs et aux associations*. Elle héberge aussi le matériel de **Ti Nuage** qui s'y réunit tous les mois.

Le lieu favorise les rencontres et les échanges. À l'image de **Ti Nuage**, tous les adhérents sont invités à participer à son fonctionnement.
 

## Je ne suis pas une data

L'association **[UFC - Que Choisir](https://www.quechoisir.org/combat-ufc-que-choisir-premiere-association-de-consommateurs-de-france-t3075)** a lancé une campagne visant à ouvrir les yeux sur les données amassées par les grandes entreprises du Web : [Je ne suis pas une data](https://www.jenesuispasunedata.fr). Elle fournit des conseils pour la sécurisation de vos réseaux et de vos machines, des outils pour l'analyse des données collectées sur vous sur différents sites, et des articles sur les actions menés en justice sur le sujet.

Pour mémoire, vous avez le droit de demander à toute entreprise opérant sur le Web le détail exact de toutes les données recueillies en rapport avec votre usage de ses services. En cas de difficultés, **UFC - Que Choisir** peut vous accompagner dans vos demandes et vous apporter une assistance juridique.

## Faites du Numérique

[Nous en parlons depuis quelques mois](https://www.letelegramme.fr/cotes-darmor/plougrescant/plougrescant-prepare-une-fete-numerique-hors-normes-28-12-2022-13249219.php), cette *Faites du Numérique* aura lieu dans la salle polyvalente Michel-le-Saint de Plougrescant toute l'après-midi du 11 mars. Son objet est de permettre aux visiteurs de découvrir différents usages et implications du numérique, [en dehors des écrans](https://www.patginformatique.bzh/component/zoo/item/faites-du-numerique?Itemid=101
).

Pour **Ti Nuage** il s'agira d'expliquer au public ce qu'est un *logiciel libre*, quand on entend beaucoup chez les autres intervenants parler de *logiciels gratuits*. Nous pourrons aussi faire découvrir aux visiteurs l'intérêt d'utiliser des services numériques indépendants et la richesse de l'offre logicielle libre pour tous les usages.

## Réunions de l'association

Le compte-rendu de notre dernière réunion, le jeudi 2 février dernier, est [disponible ici](/cr/2023/cr-20230202.pdf). Cette soirée a été l'occasion de faire un point sur les usages et les problèmes rencontrés sur les outils de courriels avec Isa de la Convergence des Loutres.

Notre prochaine réunion aura lieu le **2 mars** à la Hutte, comme il se doit. Vous pouvez d'ores et déjà indiquer votre présence, proposer des sujets à aborder ou poser vos questions sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-03-reunion-mensuelle).
