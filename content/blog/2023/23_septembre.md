---
title: Rentrée
date: 2023-09-23
author: David Soulayrol
---

## Réunions de l'association

Le compte-rendu de notre dernier premier jeudi est [disponible ici](/cr/2023/cr-20230907.pdf).

La prochaine se tiendra le **5 octobre**, à la Hutte bien sûr. L'ordre du jour se construit sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-10-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir abordés.

## Actualité du collectif

Le forum des CHATONS a été fortement secoué par une proposition issue d'un petit groupe de réflexion qui visait à modifier la charte pour clarifier des questions relatives aux engagement de chaque membre en ce qui concerne :

- les fournisseurs tiers en terme d'hébergement, de réseau, de nom de domaine, etc. ;
- l'utilisation de logiciel privateurs dans certaines conditions.

Le constat de certains membres du collectif était en effet que des termes de la charte n'étaient pas suffisament précis et qu'il existe par ailleurs des entorses à quelques règles ici ou là. Ces entorses peuvent s'expliquer de différentes manières ; par exemple par la difficulté d'utiliser une solution libre dans certains contextes particuliers, ou par le choix de conserver une zone grise permettant de faciliter l'arrivée de nouveaux entrants.

À l'instar de plusieurs autres membres, nous pensons que les modifications proposées vont trop loin, affaiblissent l'esprit de la charte, et ont été soumises au vote de manière précipitée, avant que ce travail soit relu et discuté largement, en dehors du groupe qui l'a initié. Par conséquent, **Ti Nuage** a choisi de voter contre cette modification.

Ces modifications ont depuis été acceptées par une majorité de votants, mais elles ont profondément divisés les membres, certains se retranchant du collectif. L'attitude de **Ti Nuage** doit être discutée en réunion. Mais nous restons quoi qu'il arrive attachés aux valeurs qui ont donné naissance aux CHATONS, et nous continuerons de manière stricte de n'utiliser que du logiciel libre.

## Autres événements

Nous n'avons pas été suffisamment rapides pour organiser une présence durant le forum aux associations comme l'an dernier. Cependant, celui de Lannion a été l'occasion de revoir Éric d'Infothema et Florian du FabLab. Ce dernier cherche à organiser régulièrement des rencontres et des conférences. Aussi, si vous avez une idée de sujet, n'hésitez pas.

Par ailleurs, notre Assemblée Générale doit prochainement avoir lieu. Cela se passera probablement en novembre, et pas le premier jeudi du mois parce que le secrétaire est indisponible. La date envisagée est donc le 9 novembre. Cela sera confirmé en octobre, et bien sûr annoncé aux adhérents par courriel.
