---
title: Nouvelle rencontre et subventions
date: 2023-06-18
author: David Soulayrol
---

## Rencontre numérique à Lannion

Nous étions conviés le mercredi 7 juin à la Maison du département à Lannion pour une rencontre autour de :

- la politique numérique départementale ;
- le réseau Construire le numérique en Côtes d'Armor ;
- la Numérithèque des Côtes d'Armor et ses modalités d'accès ;
- les appels à projets numériques, avec un bilan et un retour d'expériences des lauréats.

C'est Nourdine seul qui a fait acte de présence cette fois-ci, merci à lui. L'occasion lui a permis de revoir Grégoire et Florian du FabLab et de faire connaissances avec une poignée d'autres personnes, dont la directrice de la maison du département qui suit avec intérêt les actions menées par les <abbr title="Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires">Chatons</abbr>.

## Subvention du FDVA

Nous avons appris le 6 juin qu'une subvention du <abbr title="Le Fonds pour le Développement de la Vie Associative">FDVA</abbr> était octroyée à l'assiocation. Merci à Roxane qui travaille sur les recherches de financement pour nous.

## Le Tour des Chatons

Il est en basse Normandie un homme qui fournit un travail d'analyse toujours plus remarquable chaque année sur les hébergeurs faisant partie des Chatons. Il s'agit de Fabrice Noel qui, quand d'autres suivent les deux roues sur les routes fait lui le Tour de ces hébergeurs pour donner un aperçu qualitatif des services proposés.

Commençant avec une [simple synthèse en 2021](https://forum.chatons.org/t/meteo-pourrie-ce-week-end-jai-donc-visite-79-chatons-et/2181), il livrait l'an dernier un [document détaillé](https://forum.chatons.org/t/chatons-2022-le-tour/3972) particulièrement intéressant sur, par exemple, l'ergonomie ou le respect de la charte des différents Chatons.

Le Tour 2023 est déjà parti et devrait arriver à la mi-juillet, non pas sur les Champs Élysées mais sur [le forum](https://forum.chatons.org). On en reparle donc bientôt.

## Réunions de l'association

Le compte-rendu de notre dernier premier jeudi est [disponible ici](/cr/2023/cr-20230601.pdf). La soirée a réuni les suspects usuels pour un état des lieux sur les commandes de matériel et les événements récents.

Notre prochaine réunion aura lieu à la veille des vacances scolaires, et un jeudi comme il se doit, c'est à dire le **6 juillet**, toujours à la Hutte. L'ordre du jour figure sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-07-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir aborder.
