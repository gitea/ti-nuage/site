---
title: Libre en fête
date: 2024-03-10
author: David Soulayrol
---

L'édition 2024 de [Libre en fête](https://libre-en-fete.net/2024/) a commencé hier et va durer un mois. Ainsi, une nouvelle fois encore, des événements de découverte du Logiciel Libre et de la culture libre vont accompagner l'arrivée du printemps.

Nous participons à cet événement de deux manières. D'abord dès samedi 16 mars en proposant une nouvelle discussion au [café Théodore](http://www.cafetheodore.fr) autour, cette fois, des Conditions Générales d'Utilisation (CGU), ces petites lignes que l'on est censé lire quand on veut s'abonner à un service, et qui sont les plus courtes possible [chez nous](https://www.ti-nuage.fr/cgu.html). Cette rencontre est encore une fois à l'initiative de l'association [Tohu-Bohu](https://www.tohu.eu/) et est également annoncé sur le [programme du café](https://www.cafetheodore.fr/agenda/rencontres-autour-du-numerique-3/).

Ensuite, nous serons présents pour accompagner Nourdine qui propose le 24 mars, dès 9 heures à la salle Victor Hugo de Vieux-Marché, un *Repair Café* éphémère, c'est à dire un lieu ou il est possible d'apporter votre sèche-cheveux ou votre grille-pain récalcitrant pour le remettre en état plutôt que d'en changer. Ce café sera aussi le moment pour une *install party*, c'est à dire un atelier de discussion et d'installation de systèmes libres sur votre ordinateur (si vous prévoyez d'installer linux sur un ordinateur, veillez à sauvegarder vos données sur un disque dur externe). 

## Prochains jeudis

Ce 7 mars se tenait notre troisième réunion de l'année. Une réunion à l'affluence surprenante et mémorable. Le compte-rendu est [disponible ici](/cr/2024/cr-20240307.pdf).

Notre prochaine rencontre régulière aura lieu le **4 avril**, toujours à la Hutte. L'ordre du jour se construit sur [ce pad](https://apps.ti-nuage.fr/pad/p/2024-04-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir aborder.
