---
title: Nouvelle année, nouvelle conférence
date: 2024-01-09
author: David Soulayrol
---
Il n'échappera à personne sans doute que nous avons changé le dernier chiffre sur la date. Commençons donc par vous souhaiter à tous une heureuse nouvelle année et une plus grande maîtrise de vos données et de vos outils.

## Conférence au café Théodore

Nous nous y étions rendus le 25 novembre dernier ; des membres de [l'association Tohu-Bohu](https://www.cafetheodore.fr/tohu-bohu/), animateurs d'ateliers numériques, proposaient une première conférence au sujet du courriel et du « cloud ». Ils nous invitent le 20 janvier prochain pour discourir sur les logiciels libres. Cela se passera bien entendu **au [café Théodore](http://www.cafetheodore.fr), de 10h30 à 12h**. Venez nombreux, l'ambiance y est chaleureuse.

## Coupure du 3 janvier

À la veille de notre rendez-vous mensuel, certains d'entre vous ont peut-être rencontré des problèmes pour accéder aux services de **Ti Nuage**. De fait, les problèmes étaient bien plus amples, et tous les opérateurs télécom à l'échelle de la Bretagne ont subi des ruptures de connexion, à cause semble-t-il d'actes malveillants sur des fibres optiques. En définitive, nos services ont été inaccessible de 3 heures du matin à 17 heures environ ce 3 janvier.

## Équilibre financier

Comme détaillé lors de la dernière Assemblée Générale, les comptes de l'association sont équilibrés. Cependant, il s'avère en analysant de plus près les finances qu'il en faudrait peu pour que nous prenions le chemin d'une balance négative. Une plus grande sérénité serait atteinte si l'association accueillait une poignée d'adhérents supplémentaires. Aussi, merci de songer à renouveller votre cotisation lorsque vous recevez la notification signifiant leur échéance.

## Prochains jeudis

Ce 4 janvier se tenait la première réunion de l'année, et son compte-rendu est [disponible ici](/cr/2024/cr-20240104.pdf).

Notre prochaine rencontre régulière aura lieu le **1er février**, toujours à la Hutte. L'ordre du jour se construit sur [ce pad](https://apps.ti-nuage.fr/pad/p/2024-02-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir aborder.
