---
permalink: false
title: Rencontres
---
Outre l'accueil offert par l'association le premier jeudi de chaque mois, Ti Nuage va aussi à la rencontre d'autres lieux pour présenter et discuter les thèmes qui lui sont chers : la liberté de l'utilisateur de services numériques et la protection de ses données.

Voici les documents utilisés comme support lors des rencontres, conférences et autres causeries faites par l'association. Tous sont disponibles selon la licence [CC-BY-SA version 2.0 ou ultérieure](https://creativecommons.org/licenses/by-sa/2.0/fr/). Leurs fichiers sources sont disponibles sur notre [forge logicielle](https://forge.ti-nuage.fr/gitea/ti-nuage/talks).
