---
layout: with-map.njk
permalink: false
title: Mentions légales
---
## Nous contacter

Ti Nuage est présent sur [Mastodon](https://joinmastodon.org/fr) : [@tinuage@hostux.social](https://hostux.social/@tinuage).

Ti Nuage dispose d'un [salon public](xmpp:ti-nuage@muc.ti-nuage.fr?join) sur [XMPP](https://xmpp.org/).

L'adresse électronique de contact de l'association est : contact @ ti-nuage.fr. Le courrier doit être adressé au siège social.

Il est possible de nous rencontrer tous les premiers jeudi du mois à partir de 20 heures à [la Hutte](https://lahutte.ti-nuage.fr/), l'espace de travail partagé à Vieux Marché.

<div id="map"></div>
<script>
  const el = document.getElementById('map')
  el.style['height'] = '240px'
  el.style['margin-top'] = '1em'
  const map = L.map('map').setView([48.60679, -3.44874], 17)
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
  }).addTo(map)
  L.marker([48.60679, -3.44874]).addTo(map)
</script>
<noscript>
  <a href="https://www.openstreetmap.org/?mlat=48.60678&mlon=-3.44872#map=17/48.60678/-3.44872">
    <img src="/images/map.png" style="margin-top: 1em"/>
  </a>
</noscript>

## Publication
Directeur de publication : Nourdine Gernelle, président de l'association.

**Ti Nuage** est une association loi 1901 déclarée à la sous-préfecture de Lannion le 18 octobre 2021.

Siège social : `Mairie, 11 places aux Chevaux, 22420 Le Vieux Marché - France`
<br>RNA : `W223005884`
<br>SIREN : `904 949 534 00013`

## Données

Ce site n'exploite aucun *cookie*, ne procède à aucun pistage ni ne requiert une quelconque collecte de données. Il ne nécessite en outre pas l'usage de Javascript, sauf pour l'affichage du calendrier et des cartes dynamiques.

## Propriété intellectuelle

Ce site est conçu avec le logiciel libre [Metalsmith](https://www.metalsmith.io). Les sources sont disponibles sur notre [forge logicielle](https://apps.ti-nuage.fr/gitea/ti-nuage/site).

Sauf mention contraire sur les supports utilisés, tout le contenu affiché sur le site [ti-nuage.fr](/) appartient à l'association **Ti Nuage** et est distribuable sous licence [CC-BY-SA version 2.0 ou ultérieure](https://creativecommons.org/licenses/by-sa/2.0/fr).

Les icones utilisés pour la description des services sont réalisés avec **Uicons** de [Flaticon](https://www.flaticon.com/uicons). L'icône utilisée pour marquer les liens externes fait partie du jeu d'icône [Feather](https://feathericons.com) et est distribué sous les termes de [la licence MIT](https://mit-license.org).

