const { Buffer } = require('node:buffer')
const config = require('./config.js')
const debug = require('debug')('metalsmith-tinuage-talks')
const path = require('path')

module.exports = plugin

/**
 * Un plugin pour Metalsmith qui lit une collection de fichiers PDF la section
 * rencontres du site.
 *
 * @return {Function}
 */
function plugin () {
  const IndexCreator = {
    setup: function (page) {
      this.page = page
      this.talks = []
    },

    add: function (filename) {
      const basename = path.basename(filename)
      const names = basename.substring(0, basename.lastIndexOf('.')).split('__')

      if (names.length >= 2) {
        const name = names[0].replaceAll('_', ' ')
        const location = names[1].substring(0, names[1].lastIndexOf('_')).replaceAll('_', ' ')
        const year = names[1].substring(names[1].lastIndexOf('_') + 1)

        const talk = this.getEntry(year, location, name)

        if (names.length === 3 && names[2] === ('notes')) {
          talk.notes = filename
        } else {
          talk.doc = filename
        }
      } else {
        console.log('Ignoring talk ' + filename)
      }
    },

    fill: function (files) {
      let html = ''

      this.talks
        .reduce(
          (map, talk) => {
            (map.get(talk.year) || map.set(talk.year, []).get(talk.year)).push(talk)
            return map
          },
          new Map())
        .forEach((talks, year) => {
          html += '<h2>' + year + '</h2>\n'
          talks
            .sort((a, b) => a.year - b.year || a.title - b.title)
            .forEach(talk => {
              html += '<p>' + talk.title + ', ' + talk.location + '&nbsp;: <a href="/' + talk.doc + '">Présentation</a>, <a href="/' + talk.notes + '">Notes</a></p>\n'
            })
        })

      this.page.contents = Buffer.concat([
        this.page.contents,
        Buffer.from(html)
      ])
    },

    getEntry: function (year, location, title) {
      let talk = this.talks.find(t => t.year === year && t.location === location && t.title === title)

      if (talk == null) {
        talk = { location, title, year }
        this.talks.push(talk)
      }

      return talk
    }
  }

  return function (files, metalsmith, done) {
    if (config.paths.talksRoot) {
      const creator = Object.create(IndexCreator)

      creator.setup(files['rencontres/index.html'])

      Object.keys(files).forEach(filename => {
        if (/^rencontres\/.*\.pdf$/.test(filename)) {
          debug('Handling', filename)
          creator.add(filename)
        }
      })

      creator.fill()
    }

    done()
  }
}
